package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Company;
import com.SoftwareDevelopment.TrComp.models.Country;
import com.SoftwareDevelopment.TrComp.services.CompanyService;
import com.SoftwareDevelopment.TrComp.services.CountryService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/company")
public class CompanyControler {
    public CompanyControler(CompanyService companyService, CountryService countryService) {
        this.companyService = companyService;
        this.countryService = countryService;

    }
    private CompanyService companyService;
    private CountryService countryService;


    @GetMapping("/list")
    public String showAllCompanies(Pageable page, Model model){

        model.addAttribute("company", companyService.findAll(page));

        return "company/show-all-company.html";
    }

    @GetMapping("/delete")
    public String companyDelete(@RequestParam("id_cmp") int id) {
        companyService.deleteById(id);

        return "redirect:/company/list";
    }

    @GetMapping("/add")
    public String companyAdd(Model model) {

        Company company = new Company();

        model.addAttribute("company", company);

        return "company/update-form";
    }

    @GetMapping("/update")
    public String companyUpdate(@RequestParam("id_cmp") Integer id, Model model) {

        model.addAttribute("company", companyService.findById(id));

        return "company/update-form";
    }

    @PostMapping("/save")
    public String companySave(@ModelAttribute("company") Company company) {
        companyService.save(company);
        return "redirect:/company/list";
    }

    @GetMapping("/addCountry")
    public String addCountryCompany (@RequestParam("id_cmp") Integer id_cmp, @RequestParam("id_ctry") Integer id_ctry){
        Company company = companyService.findById(id_cmp);
        Country country = countryService.findById(id_ctry);
        company.setCountry(country);
        country.getCompanies().add(company);
        companyService.save(company);
        countryService.save(country);
        return "redirect:/company/list";
    }

    @GetMapping("/country")
    public String countryList(@RequestParam("id_cmp") Integer id_cmp, Model model){

        model.addAttribute("country", countryService.findAll());

        model.addAttribute("company", companyService.findById(id_cmp));

        return  "company/all-country";
    }
}

