package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Country;
import com.SoftwareDevelopment.TrComp.services.CompanyService;
import com.SoftwareDevelopment.TrComp.services.CountryService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/country")
public class CountryControler {
    public CountryControler(CompanyService companyService, CountryService countryService) {
        this.companyService = companyService;
        this.countryService = countryService;

    }
    private CompanyService companyService;
    private CountryService countryService;

    @GetMapping("/list")
    public String showAllCountry(Pageable page, Model model){

        model.addAttribute("country", countryService.findAll(page));

        return "country/show-all-country.html";
    }

    @GetMapping("/delete")
    public String countryDelete(@RequestParam("id_ctry") int id) {
        countryService.deleteById(id);

        return "redirect:/country/list";
    }

    @GetMapping("/add")
    public String countryAdd(Model model) {

        Country country = new Country();

        model.addAttribute("country", country);

        return "country/update-form";
    }

    @GetMapping("/update")
    public String countryUpdate(@RequestParam("id_ctry") Integer id, Model model) {

        model.addAttribute("country", countryService.findById(id));

        return "country/update-form";
    }



    @PostMapping("/save")
    public String countrySave(@ModelAttribute("country") Country country) {
        countryService.save(country);
        return "redirect:/country/list";
    }
}

