package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Game_engine;
import com.SoftwareDevelopment.TrComp.services.Game_engineService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/game_engine")
public class Game_engineControler {
    public Game_engineControler(Game_engineService game_engineService) {
        this.game_engineService = game_engineService;
    }

    private Game_engineService game_engineService;

    @GetMapping("/list")
    public String showAllGame_engine(Model model){

        model.addAttribute("game_engine", game_engineService.findAll());

        return "game_engine/show-all-game_engine.html";
    }

    @GetMapping("/delete")
    public String game_engineDelete(@RequestParam("id") int id) {
        game_engineService.deleteById(id);

        return "redirect:/game_engine/list";
    }

    @GetMapping("/add")
    public String game_engineAdd(Model model) {

        Game_engine game_engine = new Game_engine();

        model.addAttribute("game_engine", game_engine);

        return "game_engine/update-form";
    }

    @GetMapping("/update")
    public String game_engineUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("game_engine", game_engineService.findById(id));

        return "game_engine/update-form";
    }

    @PostMapping("/save")
    public String game_engineSave(@ModelAttribute("game_engine") Game_engine game_engine) {
        game_engineService.save(game_engine);
        return "redirect:/game_engine/list";
    }
}
