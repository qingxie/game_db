package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Game_magazine;
import com.SoftwareDevelopment.TrComp.services.Game_magazineService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/game_magazine")
public class Game_magazineControler {
    public Game_magazineControler(Game_magazineService game_magazineService) {
        this.game_magazineService = game_magazineService;
    }

    private Game_magazineService game_magazineService;

    @GetMapping("/list")
    public String showAllGame_magazine(Model model){

        model.addAttribute("game_magazine", game_magazineService.findAll());

        return "game_magazine/show-all-game_magazine.html";
    }

    @GetMapping("/delete")
    public String game_magazineDelete(@RequestParam("id") int id) {
        game_magazineService.deleteById(id);

        return "redirect:/game_magazine/list";
    }

    @GetMapping("/add")
    public String game_magazineAdd(Model model) {

        Game_magazine game_magazine = new Game_magazine();

        model.addAttribute("game_magazine", game_magazineService);

        return "game_magazine/update-form";
    }

    @GetMapping("/update")
    public String game_magazineUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("game_magazine", game_magazineService.findById(id));

        return "game_magazine/update-form";
    }

    @PostMapping("/save")
    public String game_magazineSave(@ModelAttribute("game_magazine") Game_magazine game_magazine) {
        game_magazineService.save(game_magazine);
        return "redirect:/game_magazine/list";
    }
}
