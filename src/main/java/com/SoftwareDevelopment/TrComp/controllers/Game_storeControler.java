package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Game_store;
import com.SoftwareDevelopment.TrComp.services.Game_storeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/game_store")
public class Game_storeControler {
    public Game_storeControler(Game_storeService game_storeService) {
        this.game_storeService = game_storeService;
    }

    private Game_storeService game_storeService;

    @GetMapping("/list")
    public String showAllGame_store(Model model){

        model.addAttribute("game_store", game_storeService.findAll());

        return "game_store/show-all-game_store.html";
    }

    @GetMapping("/delete")
    public String game_storeDelete(@RequestParam("id") int id) {
        game_storeService.deleteById(id);

        return "redirect:/game_store/list";
    }

    @GetMapping("/add")
    public String game_storeAdd(Model model) {

        Game_store game_store = new Game_store();

        model.addAttribute("game_store", game_store);

        return "game_store/update-form";
    }

    @GetMapping("/update")
    public String game_storeUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("game_store", game_storeService.findById(id));

        return "game_store/update-form";
    }

    @PostMapping("/save")
    public String game_storeSave(@ModelAttribute("game_store") Game_store game_store) {
        game_storeService.save(game_store);
        return "redirect:/game_store/list";
    }
}
