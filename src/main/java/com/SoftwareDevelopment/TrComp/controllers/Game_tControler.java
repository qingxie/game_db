package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Game_t;
import com.SoftwareDevelopment.TrComp.models.Genre;
import com.SoftwareDevelopment.TrComp.services.Game_tService;
import com.SoftwareDevelopment.TrComp.services.GenreService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/game_t")
public class Game_tControler {
    public Game_tControler(Game_tService game_tService, GenreService genreService) {
        this.genreService = genreService;
        this.game_tService = game_tService;
    }
    private GenreService genreService;
    private Game_tService game_tService;

    @GetMapping("/list")
    public String showAllGame_t(Pageable page, Model model){

        model.addAttribute("game_t", game_tService.findAll(page));

        return "game_t/show-all-game_t.html";
    }

    @GetMapping("/delete")
    public String game_tDelete(@RequestParam("id") int id) {
        game_tService.deleteById(id);

        return "redirect:/game_t/list";
    }

    @GetMapping("/add")
    public String game_tAdd(Model model) {

        Game_t game_t = new Game_t();

        model.addAttribute("game_t", game_t);

        return "game_t/update-form";
    }

    @GetMapping("/update")
    public String game_tUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("game_t", game_tService.findById(id));

        return "game_t/update-form";
    }

    @PostMapping("/save")
    public String game_tSave(@ModelAttribute("game_t") Game_t game_t) {
        game_tService.save(game_t);
        return "redirect:/game_t/list";
    }

    @GetMapping("/addGenre")
    public String addEngineGame_t (@RequestParam("id_game") Integer id_game, @RequestParam("id_genre") Integer id_genre){

        Game_t game_t = game_tService.findById(id_game);
        Genre genre = genreService.findById(id_genre);

        game_t.getGenres().add(genre);
        genre.getGame_ts().add(game_t);

        genreService.save(genre);
        game_tService.save(game_t);

        return "redirect:/game_t/list";
    }

    @GetMapping("/genre")
    public String genreList(@RequestParam("id") Integer id, Model model){

        model.addAttribute("genre", genreService.findAll());

        model.addAttribute("game_t", game_tService.findById(id));

        return  "game_t/all-genre";
    }

    @GetMapping("/selectGenre")
    public String selectGenreGame(@RequestParam("id") Integer id, Model model){

        Game_t game_t = game_tService.findById(id);

        model.addAttribute("genre", genreService.findAll());

        model.addAttribute("game_t", game_tService.findById(id));

        return "game_t/add-genre-to-game_t";
    }

    @GetMapping("/deleteGenre")
    public String sortDeleteType(@RequestParam("id_game") int id_game,@RequestParam("id_genre") int id_genre) {
        Game_t game_t = game_tService.findById(id_game);
        for(Genre s : game_t.getGenres()){
            if(s.getId()==id_genre){
                game_t.getGenres().remove(s);
                s.setGame_ts(null);
                game_tService.save(game_t);
                genreService.save(s);
                break;
            }
        }

        return "redirect:/genre/list";
    }
}
