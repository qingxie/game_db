package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Game_t;
import com.SoftwareDevelopment.TrComp.models.Genre;
import com.SoftwareDevelopment.TrComp.services.Game_tService;
import com.SoftwareDevelopment.TrComp.services.GenreService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/genre")
public class GenreControler {
    public GenreControler(GenreService genreService, Game_tService game_tService) {
        this.game_tService = game_tService;
        this.genreService = genreService;
    }
    private Game_tService game_tService;
    private GenreService genreService;

    @GetMapping("/list")
    public String showAllGenre(Pageable page, Model model){

        model.addAttribute("genre", genreService.findAll(page));

        return "genre/show-all-genre.html";
    }

    @GetMapping("/delete")
    public String genreDelete(@RequestParam("id") int id) {
        genreService.deleteById(id);

        return "redirect:/genre/list";
    }

    @GetMapping("/add")
    public String genreAdd(Model model) {

        Genre genre = new Genre();

        model.addAttribute("genre", genre);

        return "genre/update-form";
    }

    @GetMapping("/update")
    public String genreUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("genre", genreService.findById(id));

        return "genre/update-form";
    }

    @GetMapping("/addGame")
    public String addGameGenre (@RequestParam("id_genre") Integer id_genre, @RequestParam("id_game") Integer id_game){
        Genre genre = genreService.findById(id_genre);
        Game_t game_t = game_tService.findById(id_game);
        genre.getGame_ts().add(game_t);
        game_t.getGenres().add(genre);
        genreService.save(genre);
        game_tService.save(game_t);
        return "redirect:/genre/list";
    }

    @GetMapping("/game_t")
    public String gameList(@RequestParam("id") Integer id, Model model){

        model.addAttribute("game_t", game_tService.findAll());

        model.addAttribute("genre", genreService.findById(id));

        return  "genre/all-game_t";
    }

    @GetMapping("/selectGame")
    public String selectGameGenre(@RequestParam("id") Integer id, Model model){

        Genre genre = genreService.findById(id);

        model.addAttribute("game_t", game_tService.findAll());

        model.addAttribute("genre", genreService.findById(id));

        return "genre/add-game_t-to-genre";
    }

    @PostMapping("/save")
    public String genreSave(@ModelAttribute("genre") Genre genre) {

        genreService.save(genre);

        return "redirect:/genre/list";
    }

    @GetMapping("/deleteGame")
    public String sortDeleteType(@RequestParam("id_game") int id_game,@RequestParam("id_genre") int id_genre) {
        Genre genre = genreService.findById(id_genre);
        for(Game_t s : genre.getGame_ts()){
            if(s.getId()==id_game){
                genre.getGame_ts().remove(s);
                s.setGenres(null);
                genreService.save(genre);
                game_tService.save(s);
                break;
            }
        }

        return "redirect:/genre/list";
    }

}