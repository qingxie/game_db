package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Prog_3d;
import com.SoftwareDevelopment.TrComp.services.Prog_3dService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/prog_3d")
public class Prog_3dControler {
    public Prog_3dControler(Prog_3dService prog_3dService) {
        this.prog_3dService = prog_3dService;
    }

    private Prog_3dService prog_3dService;

    @GetMapping("/list")
    public String showAllProg_3d(Model model){

        model.addAttribute("prog_3d", prog_3dService.findAll());

        return "prog_3d/show-all-prog_3d.html";
    }

    @GetMapping("/delete")
    public String prog_3dDelete(@RequestParam("id") int id) {
        prog_3dService.deleteById(id);

        return "redirect:/prog_3d/list";
    }

    @GetMapping("/add")
    public String prog_3dAdd(Model model) {

        Prog_3d prog_3d = new Prog_3d();

        model.addAttribute("prog_3d", prog_3d);

        return "prog_3d/update-form";
    }

    @GetMapping("/update")
    public String prog_3dUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("prog_3d", prog_3dService.findById(id));

        return "prog_3d/update-form";
    }

    @PostMapping("/save")
    public String prog_3dSave(@ModelAttribute("prog_3d") Prog_3d prog_3d) {
        prog_3dService.save(prog_3d);
        return "redirect:/prog_3d/list";
    }
}
