package com.SoftwareDevelopment.TrComp.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Country {
    @Id
    @Column(name = "id_ctry")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name_ctry")
    private String name;

    @OneToMany(fetch = FetchType.LAZY ,mappedBy = "country",
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    private List<Company> Companies;

    public Country() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Company> getCompanies() {
        return Companies;
    }

    public void setCompanies(List<Company> companies) {
        Companies = companies;
    }

    @PreRemove
    private void preRemove() {
        for (Company s : Companies) {
            s.setCountry(null);
        }
    }
}
