package com.SoftwareDevelopment.TrComp.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Game_t {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    @JoinTable(name = "game_genre",
            joinColumns = { @JoinColumn(name = "id_game", referencedColumnName = "id")},
            inverseJoinColumns = { @JoinColumn(name = "id_genre", referencedColumnName = "id")})
    private Set<Genre> Genres;

    public Game_t(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Genre> getGenres() {
        return Genres;
    }

    public void setGenres(Set<Genre> Genres) {
        this.Genres = Genres;
    }

    @PreRemove
    private void preRemove() {
        for (Genre s : Genres){
            s.getGame_ts().remove(this);
        }
    }
}
