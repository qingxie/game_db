package com.SoftwareDevelopment.TrComp.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Genre {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;



    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "Genres",
            cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
            })
    private Set<Game_t> Game_ts;

    public Genre(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Game_t> getGame_ts() {
        return Game_ts;
    }

    public void setGame_ts(Set<Game_t> game_ts) {
        Game_ts = game_ts;
    }
}
