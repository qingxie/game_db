package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Game_engine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Game_engineRepository extends JpaRepository<Game_engine, Integer> {
}
