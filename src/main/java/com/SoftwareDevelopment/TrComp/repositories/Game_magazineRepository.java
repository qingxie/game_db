package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Game_magazine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Game_magazineRepository extends JpaRepository<Game_magazine, Integer> {
}
