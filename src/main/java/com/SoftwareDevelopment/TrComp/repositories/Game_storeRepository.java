package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Game_store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Game_storeRepository extends JpaRepository<Game_store, Integer> {
}
