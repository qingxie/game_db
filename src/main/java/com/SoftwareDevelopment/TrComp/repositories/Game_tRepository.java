package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Game_t;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Game_tRepository extends JpaRepository<Game_t, Integer> {
}
