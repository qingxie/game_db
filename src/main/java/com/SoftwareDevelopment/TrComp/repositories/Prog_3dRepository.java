package com.SoftwareDevelopment.TrComp.repositories;

import com.SoftwareDevelopment.TrComp.models.Prog_3d;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Prog_3dRepository extends JpaRepository<Prog_3d, Integer> {
}
