package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Company;
import com.SoftwareDevelopment.TrComp.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompanyService {
    @Autowired
    CompanyRepository companyRepository;
    public Company findById(Integer id) {
        Optional<Company> result = companyRepository.findById(id);
        Company n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Company> findAll() {
        return companyRepository.findAll();
    }

    public Iterable<Company> findAll(Pageable pageable) {
        return companyRepository.findAll(pageable);
    }

    public Iterable<Company> findAll(Sort sort) {
        return companyRepository.findAll(sort);

    }

    public void save(Company company) {
        companyRepository.save(company);
    }

    public void deleteById(Integer id){
        companyRepository.deleteById(id);
    }
}
