package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Country;
import com.SoftwareDevelopment.TrComp.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CountryService {
    @Autowired
    CountryRepository countryRepository;
    public Country findById(Integer id) {
        Optional<Country> result = countryRepository.findById(id);
        Country n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Country> findAll() {
        return countryRepository.findAll();
    }

    public Iterable<Country> findAll(Pageable pageable) {
        return countryRepository.findAll(pageable);
    }

    public Iterable<Country> findAll(Sort sort) {
        return countryRepository.findAll(sort);

    }

    public void save(Country country) {
        countryRepository.save(country);
    }

    public void deleteById(Integer id){
        countryRepository.deleteById(id);
    }
}
