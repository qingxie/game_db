package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Game_engine;
import com.SoftwareDevelopment.TrComp.repositories.Game_engineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Game_engineService {
    @Autowired
    Game_engineRepository game_engineRepository;
    public Game_engine findById(Integer id) {
        Optional<Game_engine> result = game_engineRepository.findById(id);
        Game_engine n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Game_engine> findAll() {
        return game_engineRepository.findAll();
    }

    public Iterable<Game_engine> findAll(Pageable pageable) {
        return game_engineRepository.findAll(pageable);
    }

    public Iterable<Game_engine> findAll(Sort sort) {
        return game_engineRepository.findAll(sort);

    }

    public void save(Game_engine game_engine) {
        game_engineRepository.save(game_engine);
    }

    public void deleteById(Integer id){
        game_engineRepository.deleteById(id);
    }
}
