package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Game_magazine;
import com.SoftwareDevelopment.TrComp.repositories.Game_magazineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Game_magazineService {
    @Autowired
    Game_magazineRepository game_magazineRepository;
    public Game_magazine findById(Integer id) {
        Optional<Game_magazine> result = game_magazineRepository.findById(id);
        Game_magazine n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Game_magazine> findAll() {
        return game_magazineRepository.findAll();
    }

    public Iterable<Game_magazine> findAll(Pageable pageable) {
        return game_magazineRepository.findAll(pageable);
    }

    public Iterable<Game_magazine> findAll(Sort sort) {
        return game_magazineRepository.findAll(sort);

    }

    public void save(Game_magazine game_magazine) {
        game_magazineRepository.save(game_magazine);
    }

    public void deleteById(Integer id){
        game_magazineRepository.deleteById(id);
    }
}
