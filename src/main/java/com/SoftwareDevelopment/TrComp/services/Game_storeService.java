package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Game_store;
import com.SoftwareDevelopment.TrComp.repositories.Game_storeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Game_storeService {
    @Autowired
    Game_storeRepository game_storeRepository;
    public Game_store findById(Integer id) {
        Optional<Game_store> result = game_storeRepository.findById(id);
        Game_store n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Game_store> findAll() {
        return game_storeRepository.findAll();
    }

    public Iterable<Game_store> findAll(Pageable pageable) {
        return game_storeRepository.findAll(pageable);
    }

    public Iterable<Game_store> findAll(Sort sort) {
        return game_storeRepository.findAll(sort);

    }

    public void save(Game_store game_store) {
        game_storeRepository.save(game_store);
    }

    public void deleteById(Integer id){
        game_storeRepository.deleteById(id);
    }
}
