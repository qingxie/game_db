package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Game_t;
import com.SoftwareDevelopment.TrComp.repositories.Game_tRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Game_tService {
    @Autowired
    Game_tRepository game_tRepository;
    public Game_t findById(Integer id) {
        Optional<Game_t> result = game_tRepository.findById(id);
        Game_t n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Game_t> findAll() {
        return game_tRepository.findAll();
    }

    public Iterable<Game_t> findAll(Pageable pageable) {
        return game_tRepository.findAll(pageable);
    }

    public Iterable<Game_t> findAll(Sort sort) {
        return game_tRepository.findAll(sort);

    }

    public void save(Game_t game_t) {
        game_tRepository.save(game_t);
    }

    public void deleteById(Integer id){
        game_tRepository.deleteById(id);
    }
}
