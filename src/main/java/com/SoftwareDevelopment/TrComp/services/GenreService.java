package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Genre;
import com.SoftwareDevelopment.TrComp.repositories.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GenreService {
    @Autowired
    GenreRepository genreRepository;
    public Genre findById(Integer id) {
        Optional<Genre> result = genreRepository.findById(id);
        Genre n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Genre> findAll() {
        return genreRepository.findAll();
    }

    public Iterable<Genre> findAll(Pageable pageable) {
        return genreRepository.findAll(pageable);
    }

    public Iterable<Genre> findAll(Sort sort) {
        return genreRepository.findAll(sort);

    }

    public void save(Genre genre) {
        genreRepository.save(genre);
    }

    public void deleteById(Integer id){
        genreRepository.deleteById(id);
    }
}
