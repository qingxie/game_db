package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Prog_3d;
import com.SoftwareDevelopment.TrComp.repositories.Prog_3dRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class Prog_3dService {
    @Autowired
    Prog_3dRepository prog_3dRepository;
    public Prog_3d findById(Integer id) {
        Optional<Prog_3d> result = prog_3dRepository.findById(id);
        Prog_3d n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Prog_3d> findAll() {
        return prog_3dRepository.findAll();
    }

    public Iterable<Prog_3d> findAll(Pageable pageable) {
        return prog_3dRepository.findAll(pageable);
    }

    public Iterable<Prog_3d> findAll(Sort sort) {
        return prog_3dRepository.findAll(sort);

    }

    public void save(Prog_3d prog_3d) {
        prog_3dRepository.save(prog_3d);
    }

    public void deleteById(Integer id){
        prog_3dRepository.deleteById(id);
    }
}
